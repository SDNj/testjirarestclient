Summary
REST client with OAuth authorization can't download attachments with a UTF-8 character in the file name.

see <https://jira.atlassian.com/browse/JRACLOUD-69680>