package ru.sdnj.jira;

import java.io.IOException;
import java.util.Base64;
import java.util.logging.Logger;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.ClientRequestContext;
import javax.ws.rs.client.ClientRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import org.glassfish.jersey.client.ClientProperties;
import org.glassfish.jersey.client.JerseyClientBuilder;
import org.glassfish.jersey.filter.LoggingFilter;

public class JiraClient {

	private JiraOAuth jiraOAuth;
	private Client client;

	public JiraClient() {
	}

	public Client getClient() {
		if (client == null) {
			client = initClient();
		}
		return client;
	}

	private Client initClient() {
		client = getClientBuilder().build();
		client.property(ClientProperties.CONNECT_TIMEOUT, 5 * 60 * 1000);
		client.property(ClientProperties.READ_TIMEOUT, 10 * 60 * 1000);
		client.register(new LoggingFilter(Logger.getLogger(getClass().getName()), true));
		if (!getPassword().isEmpty()) {
			client.register(new BasicAuthenticator(getUser(), getPassword()));
		} else {
			String[] token = getAccessToken();
			client.register(instantiateOAuth().getOAuthFilter(token[0], token[1]));
		}
		return client;
	}

	public static JiraOAuth instantiateOAuth() {
		return new JiraOAuth(getClientBuilder());
	}

	private static ClientBuilder getClientBuilder() {
		return new JerseyClientBuilder();
	}

	private String[] getAccessToken() {
		return System.getProperty("oauth.token").split(",");
	}

	private String getUser() {
		return System.getProperty("user");
	}

	private String getPassword() {
		return System.getProperty("password");
	}

	private static class BasicAuthenticator implements ClientRequestFilter {

		String user;
		String password;

		public BasicAuthenticator(String user, String password) {
			this.user = user;
			this.password = password;
		}

		@Override
		public void filter(ClientRequestContext requestContext) throws IOException {
			requestContext.getHeaders().add(HttpHeaders.AUTHORIZATION, "Basic "
					+ Base64.getEncoder().encodeToString((user + ':' + password).getBytes()));
		}
	}
}
