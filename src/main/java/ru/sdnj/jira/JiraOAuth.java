package ru.sdnj.jira;

import java.util.Map;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.Feature;

import org.glassfish.jersey.client.oauth1.AccessToken;
import org.glassfish.jersey.client.oauth1.ConsumerCredentials;
import org.glassfish.jersey.client.oauth1.OAuth1AuthorizationFlow;
import org.glassfish.jersey.client.oauth1.OAuth1Builder;
import org.glassfish.jersey.client.oauth1.OAuth1ClientSupport;
import org.glassfish.jersey.oauth1.signature.OAuth1Parameters;
import org.glassfish.jersey.oauth1.signature.RsaSha1Method;

public class JiraOAuth {

	private String clientId;
	private String clientSecret;
	private String baseUrl;
	private String redirectUrl;
	private ClientBuilder clientBuilder;
	private String token;
	OAuth1AuthorizationFlow authFlow;

	public JiraOAuth(ClientBuilder clientBuilder) {
		this.clientId = System.getProperty("oauth.clientId");
		this.clientSecret = System.getProperty("oauth.clientSecret");
		this.baseUrl = System.getProperty("baseUrl");
		this.redirectUrl = System.getProperty("oauth.redirectUrl");
		this.clientBuilder = clientBuilder;
	}

	public String extractRequestId(Map<String, String> response) {
		return response.get(OAuth1Parameters.TOKEN);
	}

	public String generateRequest() throws Exception {
		String url = getOAuth1AuthorizationFlow().start();
		return url;
	}

	public String consumeResponse(Map<String, String> response) throws Exception {
		AccessToken accessToken = getOAuth1AuthorizationFlow().finish(response.get(OAuth1Parameters.VERIFIER));
		return accessToken.getToken() + "," + accessToken.getAccessTokenSecret();
	}

	public Feature getOAuthFilter(String token, String accessTokenSecret) {
		AccessToken storedToken = new AccessToken(token, accessTokenSecret);
		Feature filterFeature = getBuilder().feature().accessToken(storedToken).build();
		return filterFeature;
	}

	private OAuth1Builder getBuilder() {
		ConsumerCredentials consumerCredentials = new ConsumerCredentials(clientId, clientSecret);
		return OAuth1ClientSupport.builder(consumerCredentials).signatureMethod(RsaSha1Method.NAME);
	}

	private OAuth1AuthorizationFlow getOAuth1AuthorizationFlow() {
		if (authFlow == null) {
			authFlow = getBuilder().authorizationFlow(
					baseUrl + "/plugins/servlet/oauth/request-token",
					baseUrl + "/plugins/servlet/oauth/access-token",
					baseUrl + "/plugins/servlet/oauth/authorize")
					.callbackUri(redirectUrl).client(clientBuilder.build()).build();
		}
		return authFlow;
	}
}
