/*
 * To change this template file, choose Tools | Templates
 */
package ru.sdnj.jira;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import javax.ws.rs.core.Response;

import org.junit.BeforeClass;
import org.junit.Test;

public class ClientTest {

	public ClientTest() {
	}

	@BeforeClass
	public static void setUpClass() throws IOException {
		System.getProperties().load(OAuthTest.class.getResourceAsStream("/system.properties"));
	}

	@Test
	public void testOauth() {
		System.setProperty("password", "");
		testAttacments();
	}

	@Test
	public void testBasic() {
		System.setProperty("password", System.getProperty("user.password"));
		testAttacments();
	}

	private void testAttacments() {
		String attachment;

		attachment = "10017/problem_houston.jpg";
		try {
			load(attachment);
		} catch (IOException e) {
			e.printStackTrace();
			fail(attachment);
		}

		attachment = "10018/problem_houston%5B1%5D.jpg";
		try {
			load(attachment);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	protected void load(String attachment) throws IOException {
		String baseUrl = System.getProperty("baseUrl");

		JiraClient client = new JiraClient();
		Response response = client.getClient().target(baseUrl)
				.path("secure/attachment")
				.path(attachment)
				.request()
				.get();

		assertEquals(200, response.getStatus());

		InputStream input = response.readEntity(InputStream.class);

		Path file = new File(Files.createTempDirectory("TEST").toFile(), "test.jpg").toPath();
		Files.copy(input, file);
	}
}
