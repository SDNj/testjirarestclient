/*
 * To change this template file, choose Tools | Templates
 */
package ru.sdnj.jira;

import java.io.IOException;
import java.util.Arrays;
import java.util.Map;
import java.util.Scanner;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author SDNj
 */
public class OAuthTest {

	private static final Logger logger = Logger.getLogger("TEST");

	public OAuthTest() {
	}

	@BeforeClass
	public static void setUpClass() throws IOException {
		System.getProperties().load(OAuthTest.class.getResourceAsStream("/system.properties"));
	}

	@Test
	public void test() throws Exception {
		JiraOAuth oauth = JiraClient.instantiateOAuth();
		String request = oauth.generateRequest();
		logger.info(request);

		Scanner scanner = new Scanner(System.in);
		String paramsStr = scanner.nextLine();
		Map<String, String> params = Arrays.stream(paramsStr.split("&"))
				.map(p -> p.split("="))
				.collect(Collectors.toMap(a -> a[0], a -> a[1]));
		String auth = oauth.consumeResponse(params);
		logger.info(auth);
	}
}
